<?php

    $text = [
        ['x' => 100, 'y' => 100, 'angle' => 0, 'color' => '#fff', 'size' => 50, 'text' => sprintf($lang->nickname, $Client['client_nickname'])], // ник
        ['x' => 100, 'y' => 200, 'angle' => 0, 'color' => '#fff', 'size' => 50, 'text' => sprintf($lang->time, date('H:i'))], // время
        ['x' => 100, 'y' => 300, 'angle' => 0, 'color' => '#fff', 'size' => 50, 'text' => sprintf($lang->date, date('d.m.Y'))], // дата
        ['x' => 100, 'y' => 400, 'angle' => 0, 'color' => '#fff', 'size' => 50, 'text' => sprintf($lang->online, $server['now'], $server['max'])], // онлайн
    ];


    /*
        x       Горизонтальное смещение в пикселях слева от текста
        y       Вертикальное смещение в пикселях к базовому тексту
        angle   Угол под которым выводится текст. Положительное значение: направление от верхнего левого угла до нижнего правого угла. Отрицательное значение: от нижнего левого угла до верхнего правого угла.
        size    Размер шрифта
        text    Строка с текстом
    */