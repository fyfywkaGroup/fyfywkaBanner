<?php
    
    require_once './fyfywkaBanner/init.php';

    $ts3 = TeamSpeak3::factory('serverquery://'.$cfg['ts3']['login'].':'.$cfg['ts3']['pass'].'@'.$cfg['ts3']['ip'].':'.$cfg['ts3']['qport'].'/?server_port='.$cfg['ts3']['vport'].'&nickname='.urlencode($cfg['ts3']['botname']));

    $server['nowQ']   = (int) $ts3['virtualserver_queryclientsonline'];
    $server['now']    = (int) $ts3['virtualserver_clientsonline'] - $server['nowQ'];
    $server['max']    = (int) $ts3['virtualserver_maxclients'];

    foreach($ts3->clientList(['connection_client_ip' => getClientIP(), 'client_type' => '0']) as $Client) {
        break;
    }

    if (!isset($Client['client_nickname'])) {
        die('клиент не на сервере');
    }

    if(file_exists('./fyfywkaBanner/lang/'.$Client['client_country'].'.json')) {
        $lang = json_decode(file_get_contents('./fyfywkaBanner/lang/'.$Client['client_country'].'.json'));
    } else {
        $lang = json_decode(file_get_contents('./fyfywkaBanner/lang/'.$cfg['banner']['default_language'].'.json'));
    }

    if(!file_exists('./fyfywkaBanner/img/'.$cfg['banner']['img']))
        die('изображения отсутствует или неверное имя');
    $banner = new Imagick('./fyfywkaBanner/img/'.$cfg['banner']['img']);

    if(file_exists('./fyfywkaBanner/img/'.$cfg['banner']['filter'])) {
        $filter = new Imagick('./fyfywkaBanner/img/'.$cfg['banner']['filter']);
        $banner->compositeImage($filter, Imagick::COMPOSITE_DEFAULT, 0, 0);
    }

    ini_set('date.timezone', $cfg['banner']['time_zone']);

    $draw = new ImagickDraw();
    $draw->setFont('./fyfywkaBanner/font/'.$cfg['banner']['font']);

    require_once './fyfywkaBanner/config/text.php';
    foreach ($text as $value) {
        $draw->setFontSize($value['size']);
        text($draw, $value['x'], $value['y'], $value['angle'], $value['color'], $value['text']);
    }

    //$draw->setGravity(Imagick::GRAVITY_CENTER);
    //$draw->setFontSize(200);
    //text($draw, 0, -120, 0, '#fff', date('H:i'));

    $ts3->request('quit');
    $banner->setImageFormat('png');
    header("Content-type: image/png");
    echo $banner;

    function getClientIP()
    {
        $ip = false;
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif(isset($_SERVER['HTTP_X_FORWARDED']))
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        elseif(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        elseif(isset($_SERVER['HTTP_FORWARDED']))
            $ip = $_SERVER['HTTP_FORWARDED'];
        elseif(isset($_SERVER['REMOTE_ADDR']))
            $ip = $_SERVER['REMOTE_ADDR'];

        return $ip;
    }

    function text($draw, $x, $y, $z, $color, $text)
    {
        global $banner;
        $pixel = 1;

        $draw->setFillColor('#000');
        $banner->annotateImage($draw, $x+$pixel, $y, $z, $text);
        $banner->annotateImage($draw, $x, $y+$pixel, $z, $text);
        $banner->annotateImage($draw, $x-$pixel, $y, $z, $text);
        $banner->annotateImage($draw, $x, $y-$pixel, $z, $text);
        $draw->setFillColor($color);
        $banner->annotateImage($draw, $x, $y, $z, $text);
    }

